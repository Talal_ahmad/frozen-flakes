<?php

namespace App\Http\Controllers;

use SimpleSoftwareIO\QrCode\Facades\QrCode;

use Illuminate\Http\Request;

class FrozenFlakesController extends Controller
{
    public function index()
    {
        return view("frozenflakes.frozen_flakes");
    }

    public function menu()
    {
        return view("frozenflakes.menu");
    }

    public function products()
    {
        return view("frozenflakes.products");
    }

    public function about_us()
    {
        return view("frozenflakes.about_us");
    }

    public function feedbacks()
    {
        return view("frozenflakes.feedbacks");
    }
}
