<?php

namespace App\Http\Controllers;

use App\Models\Feedbacks;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class FeedbackController extends Controller
{
    public function index()
    {
        $feedbacks = Feedbacks::orderBy("created_at")->get();
        // dd($feedbacks);
        return view("frozenflakes.view_feedbacks", get_defined_vars());
    }

    public function store(Request $request)
    {
        try {
            $this->validate($request, [
                "name" => 'required',
                'email' => 'required|email|',
                'phone_no' => 'required',
                'satisfaction' => 'required',
                'suggestion' => 'required'
            ]);
            $feedback = Feedbacks::create($request->all());
            return redirect('/')->with('success', 'Feedback submitted successfully.');
        } catch (\Exception | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '422', 'errors' => $e->errors()];
            } else {
                return ['code' => '500', 'error_message' => $e->getMessage()];
            }
        }
    }
}
