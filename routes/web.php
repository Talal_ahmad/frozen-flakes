<?php

use App\Http\Controllers\FeedbackController;
use App\Http\Controllers\FrozenFlakesController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [FrozenFlakesController::class, 'index']);
Route::get('/menu', [FrozenFlakesController::class, 'menu']);
Route::get('/products', [FrozenFlakesController::class, 'products']);
Route::get('/feedbacks', [FrozenFlakesController::class, 'feedbacks']);
Route::post('/submit_feedbacks', [FeedbackController::class, 'store']);
Route::get('/view_feedbacks', [FeedbackController::class, 'index']);
Route::get('/about_us', [FrozenFlakesController::class, 'about_us']);
