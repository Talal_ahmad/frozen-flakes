<style>
    @media (max-width: 992px) {

        /* lg and above */
        .position-absolute {
            position: absolute !important;
            background: linear-gradient(118.62deg, #F5F5F5 0%, #F1F4F4 31.23%, #E8F1F2 97.83%);
            /* Change position to static on lg and above */
        }
    }

    .nav_bg {
        background: rgba(250, 211, 214, 0.84) !important
    }

    .logo_cus_sett {
        width: 85%;
    }

    .contact_us_btn:hover {
        box-shadow: 8px 8px 12px grey;
    }

    @media (max-width: 1440px) {
        .logo_cus_sett {
            width: 85%;
        }
    }

    @media (max-width: 992px) {
        .logo_cus_sett {
            width: 65%;
        }

        h5 {
            font-size: 1rem !important;
        }
    }

    @media (max-width: 450px) {
        .logo_cus_sett {
            width: 40%;
        }

        .position-absolute {
            position: absolute !important;
        }

        h5 {
            font-size: 0.8rem !important;
        }

        .nav-link {
            padding: 3px !important;
        }

        .contact_us_btn {
            font-size: 0.9rem !important;
        }

        .navbar-toggler {
            font-size: 0.8rem !important;
        }
    }
</style>
@if (request()->is('/'))
<div class=" position-absolute top-0 w-100 p-0 nav_bg">
     @else
        <div class="w-100 nav_bg">
@endif
    <nav class="navbar navbar-expand-lg navbar-light">
        <div class="container">
            <h3 class="m-0">
                <a class="white-space-no-wrap text-decoration-none theme_color_pink fw-bold "
                    href="{{ asset('/') }}"><img class="logo_cus_sett" height="100%"
                        src="{{ asset('assets/frozenflakes_logo.png') }}" alt=""></a>
            </h3>
            <button class="navbar-toggler px-1 px-sm-2 " type="button" data-bs-toggle="collapse"
                data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse mx" id="navbarSupportedContent">
                <ul class="navbar-nav mx-auto ">
                    <li class="nav-item px px-1 {{ request()->is('/') ? 'active' : '' }} text-lg-start text-center">
                        <h5><a class="nav-link fw-bold text-secondary {{ request()->is('/') ? 'text-decoration-underline' : '' }} "
                                href="{{ asset('/') }}">Home</a></h5>
                    </li>
                    <li class="nav-item px-1 {{ request()->is('about_us') ? 'active' : '' }} text-lg-start text-center">
                        <h5>
                            <a class="nav-link fw-bold text-secondary  {{ request()->is('about_us') ? 'text-decoration-underline' : '' }} "
                                href="{{ asset('about_us') }}">About Us</a>
                        </h5>
                    </li>
                    <li class="nav-item px-1 {{ request()->is('menu') ? 'active' : '' }} text-lg-start text-center">
                        <h5>
                            <a class="nav-link fw-bold  text-secondary {{ request()->is('menu') ? 'text-decoration-underline' : '' }} "
                                href="{{ asset('menu') }}">Menu</a>
                        </h5>
                    </li>
                    <li
                        class="nav-item px-1 {{ request()->is('products') ? 'active' : '' }} text-lg-start text-center">
                        <h5>
                            <a class="nav-link fw-bold  text-secondary {{ request()->is('products') ? 'text-decoration-underline' : '' }} "
                                href="{{ asset('products') }}">Products</a>
                        </h5>
                    </li>
                    <li
                        class="nav-item px-1 {{ request()->is('feedbacks') ? 'active' : '' }} text-lg-start text-center">
                        <h5>
                            <a class="nav-link fw-bold  text-secondary {{ request()->is('feedbacks') ? 'text-decoration-underline' : '' }} "
                                href="{{ asset('feedbacks') }}">Feedback</a>
                        </h5>
                    </li>
                </ul>
                <div class="d-flex align-items-center justify-content-evenly ">
                    {{-- <img class="btn custom-nav-btn-width p-0 mx-2" src="{{ asset('assets\Group 61492.png') }}"
                    alt="Search">
                <img class="btn custom-nav-btn-width p-0 mx-2" src="{{ asset('assets\Group 61493.png') }}"
                    alt="Add to Cart"> --}}
                    <a href="#contact_us">
                        <button class="btn contact_us_btn white-space-no-wrap rounded-pill px-sm-3 px-2 py-sm-2 py-1"
                            type="submit">Contact
                            Us</button>
                    </a>
                </div>
            </div>
        </div>
    </nav>
</div>
