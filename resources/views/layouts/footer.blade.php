<style>
    .fs_custom_comments_top {
        font-size: 1.25rem !important
    }

    .custo_w_footer_contact_us_address {
        width: 75% !important;
    }

    .custom_footer_copyright_margin {
        margin-top: 3rem !important;
    }

    @media only screen and (max-width: 768px) {
        .fs_custom_comments_top {
            font-size: 1rem !important;
        }

        .custo_w_footer_contact_us_address {
            width: 100% !important;
        }

        .custom_footer_copyright_margin {
            margin-top: 2rem !important;
        }
    }

    @media only screen and (max-width: 375px) {
        .fs_custom_comments_top {
            font-size: 0.7rem !important;
        }

        .custo_w_footer_contact_us_address {
            width: 100% !important;
        }

        .custom_footer_copyright_margin {
            margin-top: 1.5rem !important;
        }
    }
</style>

<footer class=" ff_popins m-0 p-0 mt-5" id="contact_us">
    <div class=" mx-md-5 mx-2">
        <div
            class="row justify-content-center justify-content-md-start row-cols-1 row-cols-sm-3 text-center text-sm-start mx-2 mx-sm-auto ">
            {{-- <div class="col-1"></div> --}}
            <div class="col-md-4s col-12s text-center text-md-start mb-4 mb-md-auto ">
                <p class=" theme_color_pink fw-bold fs-5 mb-md-1">Frozen Flakes Cafe</p>
                <p class="col-xl-10 col-lg-12 col-md-11 col-sm-12 responsive_font_footer text-secondary fs-5">Join Us On
                    A Delicious
                    Adventure
                    Where Every Scoop Tells A Tale Of Craftsmanship And
                    Delight And Delicious Falvours.
                </p>
                <i class="fa-brands fa-facebook theme_color_pink fs-3 px-1"></i>
                <i class="fa-brands fa-twitter theme_color_pink fs-3 px-1"></i>
                <i class="fa-brands fa-linkedin theme_color_pink fs-3 px-1"></i>
                <a href="https://www.instagram.com/frozenflakescafe?utm_source=qr&igsh=MW96b2dvZDdyaGxicw=="
                    target="_blank"><i class="fa-brands fa-instagram theme_color_pink fs-3 px-1"></i></a>

            </div>
            {{-- <div class=" col-5 col-md-2 ">
                <p class=" fw-bold text-dark responsive_font_footer">Shop</p>
                <p class=" text-secondary responsive_font_footer_2">Cone</p>
                <p class=" text-secondary responsive_font_footer_2">Chocolate Bar</p>
                <p class=" text-secondary responsive_font_footer_2">Cup Cake</p>
                <p class=" text-secondary responsive_font_footer_2">Ice Cream</p>
            </div> --}}
            <div class=" col-7s col-md-2s ">
                <p class=" fw-bold text-dark responsive_font_footer ">Contact Us</p>
                <p class=" text-secondary responsive_font_footer_2"><i class="fa fa-phone"
                        aria-hidden="true"></i>&nbsp;+971-52-891-4440</p>
                <p class=" text-secondary responsive_font_footer_2 custo_w_footer_contact_us_address"><i
                        class="fa fa-location-arrow" aria-hidden="true"></i>&nbsp;Al Rafah - Sheikh Mohammed bin Salem
                    St - Ras
                    al Khaimah - United
                    Arab Emirates.</p>
                <p class=" text-secondary responsive_font_footer_2"><i class="fa fa-mail-forward"
                        aria-hidden="true"></i>&nbsp;info@frozenflakes.com</p>
            </div>
            <div class=" col-12s col-md-4s p-0 d-flexs justify-content-around align-items-center ">
                @php
                    $url = 'https://www.frozenflakes.com/';
                    $logoPath = public_path('assets/frozenflakes_logo.png');
                    // dd($logoPath);
                    $qrCode = QrCode::style('round')->eye('circle')->color(247, 141, 150)->size(100)->generate($url);
                @endphp

                <div class=" col-12s mx-auto">

                    <p class="mb-1">
                        <b
                            class=" text-start text-decoration-underline fw-bold fs_custom_comments_top">Instructions:</b>
                        <span class=" text-secondary responsive_font_footer_2 mb-1 text-center">To install <b>Frozen
                                Flakes</b> app. Simply, open Google lens your on mobile
                            phone. Scan the QR
                            code and
                            wait for it to be recognized. Click the link and install
                            the app.</span>
                    </p>
                </div>
                <div class="mb-0 m-0 p-0 text-center ">
                    {!! $qrCode !!}
                </div>

                {{-- <p class=" fw-bold text-secondary responsive_font_footer">Get Online App</p>
                <a href="#" target="_blank">
                    <img class="mb-1" width="100%" height="auto" src="{{ asset('assets/image 1.png') }}"
                        alt="">
                </a>
                <a href="#" target="_blank">
                    <img width="100%" height="auto" src="{{ asset('assets/image 2.png') }}" alt="">
                </a> --}}
            </div>
        </div>
    </div>
    <div
        class="border border-top border-bottom-0 border-start-0 border-end-0 custom_footer_copyright_margin p-1 border-secondary-subtle ">
        <p class=" text-center pt-3 copyright_footer">Copyright © 2023 Frozenflakescafe All Rights Reserved. Powered By
            Bright-Line
            Solutions.</p>
    </div>
</footer>
