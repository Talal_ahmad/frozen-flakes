<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Talal Ahmad">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>@yield('title')</title>
    <meta name="title" content="#FrozenFlakes">
    <!-- PWA -->
    <meta name="theme-color" content="#6777ef" />
    <link rel="apple-touch-icon" href="{{ asset('assets/frozenflakes_logo.png') }}">
    <link rel="manifest" href="{{ asset('/manifest.json') }}">
    <link rel="shortcut icon" href="{{ asset('assets/favicon.ico') }}" type="image/x-icon">
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"></script>
    {{-- Font Awesome --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css"
        integrity="sha512-DTOQO9RWCH3ppGqcWaEA1BIZOC6xxalwEsw9c2QQeAIftl+Vegovlnee1c9QX4TctnWMn13TZye+giMm8e2LwA=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    {{-- Bootstrap Icons --}}
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">

    <style>
        .about_us_section {
            background-image: url("{{ asset('assets/Union.png') }}");
            background-position: center !important;
            background-size: 100% 100% !important;
            background-repeat: no-repeat !important;
        }

        .specials_us_section {
            background-image: url("{{ asset('assets/Rectangle35png') }}");
            background-position: center !important;
            background-size: 100% 100% !important;
            background-repeat: no-repeat !important;
        }
    </style>
    @yield('style')
</head>

<body>
    <div class="loader-overlay"></div>
    <div class="loader-wrapper">
        <div class="loader"></div>
    </div>

    @include('layouts.header')
    @yield('body')
    @include('layouts.social_links')
    @include('layouts.footer')
    <script>
        window.addEventListener('load', function() {
            var loaderWrapper = document.querySelector('.loader-wrapper');
            var loaderOverlay = document.querySelector('.loader-overlay');

            var delayTime = 00;

            setTimeout(function() {
                loaderWrapper.style.transition = 'opacity 0.5s ease';
                loaderWrapper.style.opacity = '0';
                setTimeout(function() {
                    loaderWrapper.style.display = 'none';
                    loaderOverlay.style.display = 'none';
                }, 500);
            }, delayTime);
        });
    </script>
    <script>
        if (window.innerWidth > 640) {
            let start = new Date().getTime();

            const originPosition = {
                x: 0,
                y: 0
            };

            const last = {
                starTimestamp: start,
                starPosition: originPosition,
                mousePosition: originPosition
            }

            const config = {
                starAnimationDuration: 1500,
                minimumTimeBetweenStars: 250,
                minimumDistanceBetweenStars: 75,
                glowDuration: 75,
                maximumGlowPointSpacing: 10,
                colors: ["248 147 156", "229 129 180"],
                sizes: ["1.4rem", "1rem", "0.6rem"],
                animations: ["fall-1", "fall-2", "fall-3"]
            }

            let count = 0;

            const rand = (min, max) => Math.floor(Math.random() * (max - min + 1)) + min,
                selectRandom = items => items[rand(0, items.length - 1)];

            const withUnit = (value, unit) => `${value}${unit}`,
                px = value => withUnit(value, "px"),
                ms = value => withUnit(value, "ms");

            const calcDistance = (a, b) => {
                const diffX = b.x - a.x,
                    diffY = b.y - a.y;

                return Math.sqrt(Math.pow(diffX, 2) + Math.pow(diffY, 2));
            }

            const calcElapsedTime = (start, end) => end - start;

            const appendElement = element => document.body.appendChild(element),
                removeElement = (element, delay) => setTimeout(() => document.body.removeChild(element), delay);

            const createStar = position => {
                const star = document.createElement("span"),
                    color = selectRandom(config.colors);

                star.className = "star fa-solid fa-star";

                star.style.left = px(position.x);
                star.style.top = px(position.y);
                star.style.fontSize = selectRandom(config.sizes);
                star.style.color = `rgb(${color})`;
                star.style.textShadow = `0px 0px 1.5rem rgb(${color} / 1)`;
                star.style.animationName = config.animations[count++ % 3];
                star.style.starAnimationDuration = ms(config.starAnimationDuration);

                appendElement(star);

                removeElement(star, config.starAnimationDuration);
            }

            const createGlowPoint = position => {
                const glow = document.createElement("div");

                glow.className = "glow-point";

                glow.style.left = px(position.x);
                glow.style.top = px(position.y);

                appendElement(glow)

                removeElement(glow, config.glowDuration);
            }

            const determinePointQuantity = distance => Math.max(
                Math.floor(distance / config.maximumGlowPointSpacing),
                1
            );

            const createGlow = (last, current) => {
                const distance = calcDistance(last, current),
                    quantity = determinePointQuantity(distance);

                const dx = (current.x - last.x) / quantity,
                    dy = (current.y - last.y) / quantity;

                Array.from(Array(quantity)).forEach((_, index) => {
                    const x = last.x + dx * index,
                        y = last.y + dy * index;

                    createGlowPoint({
                        x,
                        y
                    });
                });
            }

            const updateLastStar = position => {
                last.starTimestamp = new Date().getTime();

                last.starPosition = position;
            }

            const updateLastMousePosition = position => last.mousePosition = position;

            const adjustLastMousePosition = position => {
                if (last.mousePosition.x === 0 && last.mousePosition.y === 0) {
                    last.mousePosition = position;
                }
            };

            const handleOnMove = e => {
                const mousePosition = {
                    x: e.clientX + window.pageXOffset,
                    y: e.clientY + window.pageYOffset
                }

                adjustLastMousePosition(mousePosition);

                const now = new Date().getTime(),
                    hasMovedFarEnough = calcDistance(last.starPosition, mousePosition) >= config
                    .minimumDistanceBetweenStars,
                    hasBeenLongEnough = calcElapsedTime(last.starTimestamp, now) > config.minimumTimeBetweenStars;

                if (hasMovedFarEnough || hasBeenLongEnough) {
                    createStar(mousePosition);

                    updateLastStar(mousePosition);
                }

                createGlow(last.mousePosition, mousePosition);

                updateLastMousePosition(mousePosition);
            }

            window.onmousemove = e => handleOnMove(e);

            window.ontouchmove = e => handleOnMove(e.touches[0]);

            document.body.onmouseleave = () => updateLastMousePosition(originPosition);
        }
    </script>
    <!-- Service Worker Registration -->
    <script src="{{ asset('/sw.js') }}"></script>
    <script>
        if (!navigator.serviceWorker.controller) {
            navigator.serviceWorker.register("/sw.js")
                .then(function(reg) {
                    console.log("Service worker has been registered for scope: " + reg.scope);
                });
        }
    </script>
    @yield('script')
</body>

</html>
