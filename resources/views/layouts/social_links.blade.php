<ul class="social_links_ff text-decoration-none ">
    <li class=" text-decoration-none ">
        <a id="first_social_link" href="https://www.instagram.com/frozenflakescafe?utm_source=qr&igsh=MW96b2dvZDdyaGxicw==" target="_blank"><i class="fa-brands fa-instagram text-white custom_social_link_font_size"></i></a>
    </li>
    <li>
        <a href="#" target="_blank"><i class="fa-brands fa-facebook text-white custom_social_link_font_size"></i></a>
    </li>
    <li>
        <a href="#" target="_blank"><i class="fa-brands fa-twitter text-white custom_social_link_font_size"></i></a>
    </li>
    <li>
        <a id="last_social_link" href="#" target="_blank"><i class="fa-brands fa-linkedin text-white custom_social_link_font_size rounded-bottom-2 "></i></a>
    </li>
</ul>
