@extends('layouts.master')
@section('title', 'Products')
<style>
    .custom_p_arrow {
        left: 10%;
        bottom: -3rem;
        z-index: 100;
        width: 3% !important;
    }

    .nav_tabs_product {
        background-color: #FBF1F2 !important;
        border: none;
    }

    .nav_tabs_product.active {
        background-color: #F8939C !important;
        color: white !important;
    }

    .card {
        position: relative;
        overflow: hidden;
        aspect-ratio: 1 / 1;
    }

    .card-img-container {
        position: relative;
        overflow: hidden;
        height: 200px;
        transition: filter 0.3s ease;
    }

    .card-img-container::before {
        content: '';
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        filter: blur(10px);
        z-index: -1;
        transition: filter 0.5s ease;
        /* Corrected transition property */
    }

    .card:hover .card-img-top {
        opacity: 0.5;
        filter: blur(2px);
        cursor: pointer;
    }

    .card-body {
        position: absolute;
        bottom: -100%;
        left: 0;
        width: 100%;
        padding: 1rem;
        background-color: rgba(255, 255, 255, 0.9);
        transition: bottom 0.3s ease;
    }

    .card:hover .card-body {
        bottom: 0;
    }

    .card-title,
    .card-text {
        transition: opacity 0.3s ease;
        opacity: 0;
    }

    .card:hover .card-title,
    .card:hover .card-text {
        opacity: 1;
    }
</style>
@section('body')
    <section>
        <div class=" p-0 mb-5 pb-5" id="back_to_top">
            <img width="100%" src="{{ asset('assets/product_new_banner.png') }}" alt="">
        </div>
        {{-- <div class=" position-relative ">
            <img class=" position-absolute custom_p_arrow" src="{{ asset('assets/arow animation.gif') }}" alt="">
        </div> --}}
        <div class="text-center container ">
            <div>
                <h1 class="ff_comfortaa_new m-0 display-5"><span class="theme_color_pink ff_comfortaa_new">Products</span> That <span
                        class="theme_color_pink ff_comfortaa_new">Always</span> Make</h1>
                <h2 class=" ff_comfortaa_new">You Fall In <span class="theme_color_blue ff_comfortaa_new">Love</span></h2>
            </div>
        </div>
        <div>
            <div class=" text-center container ">
                <div class=" d-flex justify-content-between bg_theme_lgblue rounded py-3 px-5 align-items-center mt-5">
                    <p class="m-0 p-0 fw-semibold text-secondary ">ALL FROZENFLAKES PRODUCTS</p>
                    <img src="{{ asset('assets/menu.png') }}" alt="">
                </div>
                <ul class="nav nav-pills text-center d-flex justify-content-center align-items-center mb-lg-5 mb-sm-3 mt-5">
                    <li class="nav-item mx-2 mx-sm-1 mt-2 mt-sm-auto">
                        <button
                            class="nav_tabs_product btn is_active active rounded-pill mx-2 ps-2 pe-3 py-1 btn_fs_menu fw-semibold"
                            onclick="active(this, 'ice-cream')"><span><img class=" bg-white rounded-circle p-1 me-2"
                                    src="{{ asset('assets/ice-cream-cup (2).png') }}" alt=""></span>Ice
                            Cream</button>
                    </li>
                    <li class="nav-item mx-2 mx-sm-1 mt-2 mt-sm-auto">
                        <button
                            class="nav_tabs_product btn is_active rounded-pill mx-2 ps-2 pe-3 py-1 btn_fs_menu fw-semibold"
                            onclick="active(this, 'hot-coffee')"><span><img class=" bg-white rounded-circle p-1 me-2"
                                    src="{{ asset('assets/tea.png') }}" alt=""></span>Hot Coffee</button>
                    </li>
                    <li class="nav-item mx-2 mx-sm-1 mt-2 mt-sm-auto">
                        <button
                            class="nav_tabs_product btn is_active rounded-pill mx-2 ps-2 pe-3 py-1 btn_fs_menu fw-semibold"
                            onclick="active(this, 'iced-coffee')"><span><img class=" bg-white rounded-circle p-1 me-2"
                                    src="{{ asset('assets/iced-coffee.png') }}" alt=""></span>Iced
                            Coffee</button>
                    </li>
                    <li class="nav-item mx-2 mx-sm-1 mt-2 mt-sm-auto">
                        <button
                            class="nav_tabs_product btn is_active rounded-pill mx-2 ps-2 pe-3 py-1 btn_fs_menu fw-semibold"
                            onclick="active(this, 'pastry')"><span><img class=" bg-white rounded-circle p-1 me-2"
                                    src="{{ asset('assets/pastry.png') }}" alt=""></span>Pastry</button>
                    </li>
                    <li class="nav-item mx-2 mx-sm-1 mt-2 mt-sm-auto">
                        <button
                            class="nav_tabs_product btn is_active rounded-pill mx-2 ps-2 pe-3 py-1 btn_fs_menu fw-semibold"
                            onclick="active(this, 'bakery')"><span><img class=" bg-white rounded-circle p-1 me-2"
                                    src="{{ asset('assets/bakery.png') }}" alt=""></span>Bakery</button>
                    </li>
                    <li class="nav-item mx-2 mx-sm-1 mt-2 mt-sm-auto">
                        <button
                            class="nav_tabs_product btn is_active rounded-pill mx-2 ps-2 pe-3 py-1 btn_fs_menu fw-semibold"
                            onclick="active(this, 'smoothie')"><span><img class=" bg-white rounded-circle p-1 me-2"
                                    src="{{ asset('assets/smoothie.png') }}" alt=""></span>Smoothie</button>
                    </li>
                </ul>
            </div>
            <div class="container p-0 p-xl-1">
                <div class=" row row-cols-lg-4 row-cols-md-2 m-0 products_on_active" id="ice-cream">
                    <div>
                        <div class="card mx-1 border-0 my-2">
                            <img width="100%" height="100%" src="{{ asset('assets/Ice-cream4.jpg') }}"
                                class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title fw-bolder fs-5 theme_color_pink text-center">
                                    Strawberry Ice-Cream🍨</h5>
                                {{-- <div class=" d-flex justify-content-start ">
                                    <p class="card-text text-secondary fs-6 fw-bold m-0">Price: &nbsp;</p>
                                    <span class=" d-flex text-secondary card-text">
                                        <p class="m-0">$10&nbsp;</p>
                                        <p class="m-0">$15(M)&nbsp;</p>
                                    <p class="m-0">$20(L)</p>
                                    </span>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="card mx-1 border-0 my-2">
                            <img width="100%" height="100%" src="{{ asset('assets/WhatsApp1.jpg') }}"
                                class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title fw-bolder fs-5 theme_color_pink text-center ">Hot Chocolate Vanilla
                                    Ice-Cream </h5>
                                {{-- <div class=" d-flex justify-content-start ">
                                    <p class="card-text text-secondary fs-6 fw-bold m-0">Price: &nbsp;</p>
                                    <span class=" d-flex text-secondary card-text">
                                        <p class="m-0">$10&nbsp;</p>
                                        <p class="m-0">$15(M)&nbsp;</p>
                                    <p class="m-0">$20(L)</p>
                                    </span>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="card mx-1 border-0 my-2">
                            <img width="100%" height="100%" src="{{ asset('assets/WhatsApp2.jpg') }}"
                                class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title fw-bolder fs-5 theme_color_pink text-center ">Rafaello Ice Cream
                                </h5>
                                {{-- <div class=" d-flex justify-content-start ">
                                    <p class="card-text text-secondary fs-6 fw-bold m-0">Price: &nbsp;</p>
                                    <span class=" d-flex text-secondary card-text">
                                        <p class="m-0">$10&nbsp;</p>
                                        <p class="m-0">$15(M)&nbsp;</p>
                                    <p class="m-0">$20(L)</p>
                                    </span>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="card mx-1 border-0 my-2">
                            <img width="100%" height="100%" src="{{ asset('assets/WhatsApp3.jpg') }}"
                                class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title fw-bolder fs-5 theme_color_pink text-center ">Pistachio Ice Cream
                                    Gelato</h5>
                                {{-- <div class=" d-flex justify-content-start ">
                                    <p class="card-text text-secondary fs-6 fw-bold m-0">Price: &nbsp;</p>
                                    <span class=" d-flex text-secondary card-text">
                                        <p class="m-0">$10&nbsp;</p>
                                        <p class="m-0">$15(M)&nbsp;</p>
                                    <p class="m-0">$20(L)</p>
                                    </span>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="card mx-1 border-0 my-2">
                            <img width="100%" height="100%" src="{{ asset('assets/WhatsApp4.jpg') }}"
                                class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title fw-bolder fs-5 theme_color_pink text-center ">Lotus Biscoff Ice
                                    Cream</h5>
                                {{-- <div class=" d-flex justify-content-start ">
                                    <p class="card-text text-secondary fs-6 fw-bold m-0">Price: &nbsp;</p>
                                    <span class=" d-flex text-secondary card-text">
                                        <p class="m-0">$10&nbsp;</p>
                                        <p class="m-0">$15(M)&nbsp;</p>
                                    <p class="m-0">$20(L)</p>
                                    </span>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="card mx-1 border-0 my-2">
                            <img width="100%" height="100%" src="{{ asset('assets/WhatsApp5.jpg') }}"
                                class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title fw-bolder fs-5 theme_color_pink text-center ">Chocolate Ice-Cream
                                    Mini Cup</h5>
                                {{-- <div class=" d-flex justify-content-start ">
                                    <p class="card-text text-secondary fs-6 fw-bold m-0">Price: &nbsp;</p>
                                    <span class=" d-flex text-secondary card-text">
                                        <p class="m-0">$10&nbsp;</p>
                                        <p class="m-0">$15(M)&nbsp;</p>
                                    <p class="m-0">$20(L)</p>
                                    </span>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="card mx-1 border-0 my-2">
                            <img width="100%" height="100%" src="{{ asset('assets/WhatsApp6.jpg') }}"
                                class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title fw-bolder fs-5 theme_color_pink text-center ">White Chocolate
                                    Hazelnut
                                </h5>
                                {{-- <div class=" d-flex justify-content-start ">
                                    <p class="card-text text-secondary fs-6 fw-bold m-0">Price: &nbsp;</p>
                                    <span class=" d-flex text-secondary card-text">
                                        <p class="m-0">$10&nbsp;</p>
                                        <p class="m-0">$15(M)&nbsp;</p>
                                    <p class="m-0">$20(L)</p>
                                    </span>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="card mx-1 border-0 my-2">
                            <img width="100%" height="100%" src="{{ asset('assets/WhatsApp7.jpg') }}"
                                class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title fw-bolder fs-5 theme_color_pink text-center ">Strawberry & Vanilla
                                    Ice-Cream🍨</h5>
                                {{-- <div class=" d-flex justify-content-start ">
                                    <p class="card-text text-secondary fs-6 fw-bold m-0">Price: &nbsp;</p>
                                    <span class=" d-flex text-secondary card-text">
                                        <p class="m-0">$10&nbsp;</p>
                                        <p class="m-0">$15(M)&nbsp;</p>
                                    <p class="m-0">$20(L)</p>
                                    </span>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="card mx-1 border-0 my-2">
                            <img width="100%" height="100%" src="{{ asset('assets/WhatsApp8.jpg') }}"
                                class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title fw-bolder fs-5 theme_color_pink text-center ">Frizzi Pop Vanila
                                </h5>
                                {{-- <div class=" d-flex justify-content-start ">
                                    <p class="card-text text-secondary fs-6 fw-bold m-0">Price: &nbsp;</p>
                                    <span class=" d-flex text-secondary card-text">
                                        <p class="m-0">$10&nbsp;</p>
                                        <p class="m-0">$15(M)&nbsp;</p>
                                    <p class="m-0">$20(L)</p>
                                    </span>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="card mx-1 border-0 my-2">
                            <img width="100%" height="100%" src="{{ asset('assets/WhatsApp9.jpg') }}"
                                class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title fw-bolder fs-5 theme_color_pink text-center ">Tiramisu Gelato Ice
                                    Cream</h5>
                                {{-- <div class=" d-flex justify-content-start ">
                                    <p class="card-text text-secondary fs-6 fw-bold m-0">Price: &nbsp;</p>
                                    <span class=" d-flex text-secondary card-text">
                                        <p class="m-0">$10&nbsp;</p>
                                        <p class="m-0">$15(M)&nbsp;</p>
                                    <p class="m-0">$20(L)</p>
                                    </span>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                </div>
                <div class=" row row-cols-lg-4 row-cols-md-2 m-0 products_on_active d-none " id="hot-coffee">
                    <div>
                        <div class="card mx-1 border-0 my-2">
                            <img width="100%" height="100%" src="{{ asset('assets/hotcoffee1.jpg') }}"
                                class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title fw-bolder fs-5 theme_color_pink text-center ">Spanish Classic Latte
                                </h5>
                                {{-- <div class=" d-flex justify-content-start ">
                                    <p class="card-text text-secondary fs-6 fw-bold m-0">Price: &nbsp;</p>
                                    <span class=" d-flex text-secondary card-text">
                                        <p class="m-0">$10&nbsp;</p>
                                        <p class="m-0">$15(M)&nbsp;</p>
                                    <p class="m-0">$20(L)</p>
                                    </span>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="card mx-1 border-0 my-2">
                            <img width="100%" height="100%" src="{{ asset('assets/hotcoffee3.jpg') }}"
                                class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title fw-bolder fs-5 theme_color_pink text-center ">Ras Al Khaimah
                                    COFFEE</h5>
                                {{-- <div class=" d-flex justify-content-start ">
                                    <p class="card-text text-secondary fs-6 fw-bold m-0">Price: &nbsp;</p>
                                    <span class=" d-flex text-secondary card-text">
                                        <p class="m-0">$10&nbsp;</p>
                                        <p class="m-0">$15(M)&nbsp;</p>
                                    <p class="m-0">$20(L)</p>
                                    </span>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="card mx-1 border-0 my-2">
                            <img width="100%" height="100%" src="{{ asset('assets/hotcoffee4.jpg') }}"
                                class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title fw-bolder fs-5 theme_color_pink text-center ">Colimbian
                                    Coffee</h5>
                                {{-- <div class=" d-flex justify-content-start ">
                                    <p class="card-text text-secondary fs-6 fw-bold m-0">Price: &nbsp;</p>
                                    <span class=" d-flex text-secondary card-text">
                                        <p class="m-0">$10&nbsp;</p>
                                        <p class="m-0">$15(M)&nbsp;</p>
                                    <p class="m-0">$20(L)</p>
                                    </span>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="card mx-1 border-0 my-2">
                            <img width="100%" height="100%" src="{{ asset('assets/hotcoffee5.jpg') }}"
                                class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title fw-bolder fs-5 theme_color_pink text-center ">Coffee Roastery
                                </h5>
                                {{-- <div class=" d-flex justify-content-start ">
                                    <p class="card-text text-secondary fs-6 fw-bold m-0">Price: &nbsp;</p>
                                    <span class=" d-flex text-secondary card-text">
                                        <p class="m-0">$10&nbsp;</p>
                                        <p class="m-0">$15(M)&nbsp;</p>
                                    <p class="m-0">$20(L)</p>
                                    </span>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                </div>
                <div class=" row row-cols-lg-4 row-cols-md-2 m-0 products_on_active d-none " id="iced-coffee">
                    <div>
                        <div class="card mx-1 border-0 my-2">
                            <img width="100%" height="100%" src="{{ asset('assets/coldcoffee1.jpg') }}"
                                class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title fw-bolder fs-5 theme_color_pink text-center ">Iced Coffee Toffee
                                </h5>
                                {{-- <div class=" d-flex justify-content-start ">
                                    <p class="card-text text-secondary fs-6 fw-bold m-0">Price: &nbsp;</p>
                                    <span class=" d-flex text-secondary card-text">
                                        <p class="m-0">$10&nbsp;</p>
                                        <p class="m-0">$15(M)&nbsp;</p>
                                    <p class="m-0">$20(L)</p>
                                    </span>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="card mx-1 border-0 my-2">
                            <img width="100%" height="100%" src="{{ asset('assets/coldcoffee2.jpg') }}"
                                class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title fw-bolder fs-5 theme_color_pink text-center ">Iced Matcha Latte

                                </h5>
                                {{-- <div class=" d-flex justify-content-start ">
                                    <p class="card-text text-secondary fs-6 fw-bold m-0">Price: &nbsp;</p>
                                    <span class=" d-flex text-secondary card-text">
                                        <p class="m-0">$10&nbsp;</p>
                                        <p class="m-0">$15(M)&nbsp;</p>
                                    <p class="m-0">$20(L)</p>
                                    </span>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="card mx-1 border-0 my-2">
                            <img width="100%" height="100%" src="{{ asset('assets/coldcoffee3.jpg') }}"
                                class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title fw-bolder fs-5 theme_color_pink text-center ">Iced Coffee
                                </h5>
                                {{-- <div class=" d-flex justify-content-start ">
                                    <p class="card-text text-secondary fs-6 fw-bold m-0">Price: &nbsp;</p>
                                    <span class=" d-flex text-secondary card-text">
                                        <p class="m-0">$10&nbsp;</p>
                                        <p class="m-0">$15(M)&nbsp;</p>
                                    <p class="m-0">$20(L)</p>
                                    </span>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="card mx-1 border-0 my-2">
                            <img width="100%" height="100%" src="{{ asset('assets/coldcoffee4.jpg') }}"
                                class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title fw-bolder fs-5 theme_color_pink text-center ">Simple Iced Coffee
                                </h5>
                                {{-- <div class=" d-flex justify-content-start ">
                                    <p class="card-text text-secondary fs-6 fw-bold m-0">Price: &nbsp;</p>
                                    <span class=" d-flex text-secondary card-text">
                                        <p class="m-0">$10&nbsp;</p>
                                        <p class="m-0">$15(M)&nbsp;</p>
                                    <p class="m-0">$20(L)</p>
                                    </span>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="card mx-1 border-0 my-2">
                            <img width="100%" height="100%" src="{{ asset('assets/coldcoffee5.jpg') }}"
                                class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title fw-bolder fs-5 theme_color_pink text-center ">Mini Lemon Pie Coffee
                                </h5>
                                {{-- <div class=" d-flex justify-content-start ">
                                    <p class="card-text text-secondary fs-6 fw-bold m-0">Price: &nbsp;</p>
                                    <span class=" d-flex text-secondary card-text">
                                        <p class="m-0">$10&nbsp;</p>
                                        <p class="m-0">$15(M)&nbsp;</p>
                                    <p class="m-0">$20(L)</p>
                                    </span>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                </div>
                <div class=" row row-cols-lg-4 row-cols-md-2 m-0 products_on_active d-none " id="pastry">
                    <div>
                        <div class="card mx-1 border-0 my-2">
                            <img width="100%" height="100%" src="{{ asset('assets/pastry1.jpg') }}"
                                class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title fw-bolder fs-5 theme_color_pink text-center ">Delicious Caremal
                                    Cake
                                </h5>
                                {{-- <div class=" d-flex justify-content-start ">
                                    <p class="card-text text-secondary fs-6 fw-bold m-0">Price: &nbsp;</p>
                                    <span class=" d-flex text-secondary card-text">
                                        <p class="m-0">$10&nbsp;</p>
                                        <p class="m-0">$15(M)&nbsp;</p>
                                    <p class="m-0">$20(L)</p>
                                    </span>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="card mx-1 border-0 my-2">
                            <img width="100%" height="100%" src="{{ asset('assets/pastry2.jpg') }}"
                                class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title fw-bolder fs-5 theme_color_pink text-center ">Lemon Pastry
                                </h5>
                                {{-- <div class=" d-flex justify-content-start ">
                                    <p class="card-text text-secondary fs-6 fw-bold m-0">Price: &nbsp;</p>
                                    <span class=" d-flex text-secondary card-text">
                                        <p class="m-0">$10&nbsp;</p>
                                        <p class="m-0">$15(M)&nbsp;</p>
                                    <p class="m-0">$20(L)</p>
                                    </span>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="card mx-1 border-0 my-2">
                            <img width="100%" height="100%" src="{{ asset('assets/pastry3.jpg') }}"
                                class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title fw-bolder fs-5 theme_color_pink text-center ">Raspberry Cheesecake
                                </h5>
                                {{-- <div class=" d-flex justify-content-start ">
                                    <p class="card-text text-secondary fs-6 fw-bold m-0">Price: &nbsp;</p>
                                    <span class=" d-flex text-secondary card-text">
                                        <p class="m-0">$10&nbsp;</p>
                                        <p class="m-0">$15(M)&nbsp;</p>
                                    <p class="m-0">$20(L)</p>
                                    </span>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="card mx-1 border-0 my-2">
                            <img width="100%" height="100%" src="{{ asset('assets/pastry4.jpg') }}"
                                class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title fw-bolder fs-5 theme_color_pink text-center ">Banana & Milk
                                    Chocolate
                                </h5>
                                {{-- <div class=" d-flex justify-content-start ">
                                    <p class="card-text text-secondary fs-6 fw-bold m-0">Price: &nbsp;</p>
                                    <span class=" d-flex text-secondary card-text">
                                        <p class="m-0">$10&nbsp;</p>
                                        <p class="m-0">$15(M)&nbsp;</p>
                                    <p class="m-0">$20(L)</p>
                                    </span>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="card mx-1 border-0 my-2">
                            <img width="100%" height="100%" src="{{ asset('assets/pastry5.jpg') }}"
                                class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title fw-bolder fs-5 theme_color_pink text-center ">Frosty Pastry
                                    Delights
                                </h5>
                                {{-- <div class=" d-flex justify-content-start ">
                                    <p class="card-text text-secondary fs-6 fw-bold m-0">Price: &nbsp;</p>
                                    <span class=" d-flex text-secondary card-text">
                                        <p class="m-0">$10&nbsp;</p>
                                        <p class="m-0">$15(M)&nbsp;</p>
                                    <p class="m-0">$20(L)</p>
                                    </span>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="card mx-1 border-0 my-2">
                            <img width="100%" height="100%" src="{{ asset('assets/pastry6.jpg') }}"
                                class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title fw-bolder fs-5 theme_color_pink text-center ">Blueberry Tart
                                </h5>
                                {{-- <div class=" d-flex justify-content-start ">
                                    <p class="card-text text-secondary fs-6 fw-bold m-0">Price: &nbsp;</p>
                                    <span class=" d-flex text-secondary card-text">
                                        <p class="m-0">$10&nbsp;</p>
                                        <p class="m-0">$15(M)&nbsp;</p>
                                    <p class="m-0">$20(L)</p>
                                    </span>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="card mx-1 border-0 my-2">
                            <img width="100%" height="100%" src="{{ asset('assets/pastry7.jpg') }}"
                                class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title fw-bolder fs-5 theme_color_pink text-center ">Mini Pecon Tart
                                </h5>
                                {{-- <div class=" d-flex justify-content-start ">
                                    <p class="card-text text-secondary fs-6 fw-bold m-0">Price: &nbsp;</p>
                                    <span class=" d-flex text-secondary card-text">
                                        <p class="m-0">$10&nbsp;</p>
                                        <p class="m-0">$15(M)&nbsp;</p>
                                    <p class="m-0">$20(L)</p>
                                    </span>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                </div>
                <div class=" row row-cols-lg-4 row-cols-md-2 m-0 products_on_active d-none " id="bakery">
                    <div>
                        <div class="card mx-1 border-0 my-2">
                            <img width="100%" height="100%" src="{{ asset('assets/bakery1.jpg') }}"
                                class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title fw-bolder fs-5 theme_color_pink text-center ">Bakery item #1
                                </h5>
                                {{-- <div class=" d-flex justify-content-start ">
                                    <p class="card-text text-secondary fs-6 fw-bold m-0">Price: &nbsp;</p>
                                    <span class=" d-flex text-secondary card-text">
                                        <p class="m-0">$10&nbsp;</p>
                                        <p class="m-0">$15(M)&nbsp;</p>
                                    <p class="m-0">$20(L)</p>
                                    </span>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="card mx-1 border-0 my-2">
                            <img width="100%" height="100%" src="{{ asset('assets/bakery2.jpg') }}"
                                class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title fw-bolder fs-5 theme_color_pink text-center ">Bakery item #2
                                </h5>
                                {{-- <div class=" d-flex justify-content-start ">
                                    <p class="card-text text-secondary fs-6 fw-bold m-0">Price: &nbsp;</p>
                                    <span class=" d-flex text-secondary card-text">
                                        <p class="m-0">$10&nbsp;</p>
                                        <p class="m-0">$15(M)&nbsp;</p>
                                    <p class="m-0">$20(L)</p>
                                    </span>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="card mx-1 border-0 my-2">
                            <img width="100%" height="100%" src="{{ asset('assets/bakery3.jpg') }}"
                                class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title fw-bolder fs-5 theme_color_pink text-center ">Bakery item #3
                                </h5>
                                {{-- <div class=" d-flex justify-content-start ">
                                    <p class="card-text text-secondary fs-6 fw-bold m-0">Price: &nbsp;</p>
                                    <span class=" d-flex text-secondary card-text">
                                        <p class="m-0">$10&nbsp;</p>
                                        <p class="m-0">$15(M)&nbsp;</p>
                                    <p class="m-0">$20(L)</p>
                                    </span>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="card mx-1 border-0 my-2">
                            <img width="100%" height="100%" src="{{ asset('assets/bakery4.jpg') }}"
                                class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title fw-bolder fs-5 theme_color_pink text-center ">Bakery item #4
                                </h5>
                                {{-- <div class=" d-flex justify-content-start ">
                                    <p class="card-text text-secondary fs-6 fw-bold m-0">Price: &nbsp;</p>
                                    <span class=" d-flex text-secondary card-text">
                                        <p class="m-0">$10&nbsp;</p>
                                        <p class="m-0">$15(M)&nbsp;</p>
                                    <p class="m-0">$20(L)</p>
                                    </span>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="card mx-1 border-0 my-2">
                            <img width="100%" height="100%" src="{{ asset('assets/bakery5.jpg') }}"
                                class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title fw-bolder fs-5 theme_color_pink text-center ">Bakery item #5
                                </h5>
                                {{-- <div class=" d-flex justify-content-start ">
                                    <p class="card-text text-secondary fs-6 fw-bold m-0">Price: &nbsp;</p>
                                    <span class=" d-flex text-secondary card-text">
                                        <p class="m-0">$10&nbsp;</p>
                                        <p class="m-0">$15(M)&nbsp;</p>
                                    <p class="m-0">$20(L)</p>
                                    </span>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="card mx-1 border-0 my-2">
                            <img width="100%" height="100%" src="{{ asset('assets/bakery6.jpg') }}"
                                class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title fw-bolder fs-5 theme_color_pink text-center ">Bakery item #6
                                </h5>
                                {{-- <div class=" d-flex justify-content-start ">
                                    <p class="card-text text-secondary fs-6 fw-bold m-0">Price: &nbsp;</p>
                                    <span class=" d-flex text-secondary card-text">
                                        <p class="m-0">$10&nbsp;</p>
                                        <p class="m-0">$15(M)&nbsp;</p>
                                    <p class="m-0">$20(L)</p>
                                    </span>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="card mx-1 border-0 my-2">
                            <img width="100%" height="100%" src="{{ asset('assets/bakery7.jpg') }}"
                                class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title fw-bolder fs-5 theme_color_pink text-center ">Bakery item #7
                                </h5>
                                {{-- <div class=" d-flex justify-content-start ">
                                    <p class="card-text text-secondary fs-6 fw-bold m-0">Price: &nbsp;</p>
                                    <span class=" d-flex text-secondary card-text">
                                        <p class="m-0">$10&nbsp;</p>
                                        <p class="m-0">$15(M)&nbsp;</p>
                                    <p class="m-0">$20(L)</p>
                                    </span>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="card mx-1 border-0 my-2">
                            <img width="100%" height="100%" src="{{ asset('assets/bakery8.jpg') }}"
                                class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title fw-bolder fs-5 theme_color_pink text-center ">Bakery item #8
                                </h5>
                                {{-- <div class=" d-flex justify-content-start ">
                                    <p class="card-text text-secondary fs-6 fw-bold m-0">Price: &nbsp;</p>
                                    <span class=" d-flex text-secondary card-text">
                                        <p class="m-0">$10&nbsp;</p>
                                        <p class="m-0">$15(M)&nbsp;</p>
                                    <p class="m-0">$20(L)</p>
                                    </span>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="card mx-1 border-0 my-2">
                            <img width="100%" height="100%" src="{{ asset('assets/bakery9.jpg') }}"
                                class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title fw-bolder fs-5 theme_color_pink text-center ">Bakery item #9
                                </h5>
                                {{-- <div class=" d-flex justify-content-start ">
                                    <p class="card-text text-secondary fs-6 fw-bold m-0">Price: &nbsp;</p>
                                    <span class=" d-flex text-secondary card-text">
                                        <p class="m-0">$10&nbsp;</p>
                                        <p class="m-0">$15(M)&nbsp;</p>
                                    <p class="m-0">$20(L)</p>
                                    </span>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="card mx-1 border-0 my-2">
                            <img width="100%" height="100%" src="{{ asset('assets/bakery10.jpg') }}"
                                class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title fw-bolder fs-5 theme_color_pink text-center ">Bakery item #10
                                </h5>
                                {{-- <div class=" d-flex justify-content-start ">
                                    <p class="card-text text-secondary fs-6 fw-bold m-0">Price: &nbsp;</p>
                                    <span class=" d-flex text-secondary card-text">
                                        <p class="m-0">$10&nbsp;</p>
                                        <p class="m-0">$15(M)&nbsp;</p>
                                    <p class="m-0">$20(L)</p>
                                    </span>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                </div>
                <div class=" row row-cols-lg-4 row-cols-md-2 m-0 products_on_active d-none " id="smoothie">
                    <div>
                        <div class="card mx-1 border-0 my-2">
                            <img width="100%" height="100%" src="{{ asset('assets/smoothie1.jpg') }}"
                                class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title fw-bolder fs-5 theme_color_pink text-center ">Vanila White
                                    Chocolate Hot Drink
                                </h5>
                                {{-- <div class=" d-flex justify-content-start ">
                                    <p class="card-text text-secondary fs-6 fw-bold m-0">Price: &nbsp;</p>
                                    <span class=" d-flex text-secondary card-text">
                                        <p class="m-0">$10&nbsp;</p>
                                        <p class="m-0">$15(M)&nbsp;</p>
                                    <p class="m-0">$20(L)</p>
                                    </span>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="card mx-1 border-0 my-2">
                            <img width="100%" height="100%" src="{{ asset('assets/smoothie2.jpg') }}"
                                class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title fw-bolder fs-5 theme_color_pink text-center ">Blueberry Healthy
                                    Smoothie
                                </h5>
                                {{-- <div class=" d-flex justify-content-start ">
                                    <p class="card-text text-secondary fs-6 fw-bold m-0">Price: &nbsp;</p>
                                    <span class=" d-flex text-secondary card-text">
                                        <p class="m-0">$10&nbsp;</p>
                                        <p class="m-0">$15(M)&nbsp;</p>
                                    <p class="m-0">$20(L)</p>
                                    </span>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="card mx-1 border-0 my-2">
                            <img width="100%" height="100%" src="{{ asset('assets/smoothie3.jpg') }}"
                                class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title fw-bolder fs-5 theme_color_pink text-center ">Iced Spanish Smoothie
                                </h5>
                                {{-- <div class=" d-flex justify-content-start ">
                                    <p class="card-text text-secondary fs-6 fw-bold m-0">Price: &nbsp;</p>
                                    <span class=" d-flex text-secondary card-text">
                                        <p class="m-0">$10&nbsp;</p>
                                        <p class="m-0">$15(M)&nbsp;</p>
                                    <p class="m-0">$20(L)</p>
                                    </span>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="card mx-1 border-0 my-2">
                            <img width="100%" height="100%" src="{{ asset('assets/smoothie4.jpg') }}"
                                class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title fw-bolder fs-5 theme_color_pink text-center ">Oreo Ice Cream Gelato
                                    Shake
                                </h5>
                                {{-- <div class=" d-flex justify-content-start ">
                                    <p class="card-text text-secondary fs-6 fw-bold m-0">Price: &nbsp;</p>
                                    <span class=" d-flex text-secondary card-text">
                                        <p class="m-0">$10&nbsp;</p>
                                        <p class="m-0">$15(M)&nbsp;</p>
                                    <p class="m-0">$20(L)</p>
                                    </span>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="card mx-1 border-0 my-2">
                            <img width="100%" height="100%" src="{{ asset('assets/smoothie5.jpg') }}"
                                class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title fw-bolder fs-5 theme_color_pink text-center ">Sicilian Pistachio
                                    Ice Cream Shake
                                </h5>
                                {{-- <div class=" d-flex justify-content-start ">
                                    <p class="card-text text-secondary fs-6 fw-bold m-0">Price: &nbsp;</p>
                                    <span class=" d-flex text-secondary card-text">
                                        <p class="m-0">$10&nbsp;</p>
                                        <p class="m-0">$15(M)&nbsp;</p>
                                    <p class="m-0">$20(L)</p>
                                    </span>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="card mx-1 border-0 my-2">
                            <img width="100%" height="100%" src="{{ asset('assets/smoothie6.jpg') }}"
                                class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title fw-bolder fs-5 theme_color_pink text-center ">Nutella Ice Cream
                                    Shake
                                </h5>
                                {{-- <div class=" d-flex justify-content-start ">
                                    <p class="card-text text-secondary fs-6 fw-bold m-0">Price: &nbsp;</p>
                                    <span class=" d-flex text-secondary card-text">
                                        <p class="m-0">$10&nbsp;</p>
                                        <p class="m-0">$15(M)&nbsp;</p>
                                    <p class="m-0">$20(L)</p>
                                    </span>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class=" text-center pt-5 mt-5">
            <h4><button class=" btn btn-secondary theme_color_bg_pink border-0 btn-lg ">
                <a class=" text-decoration-none text-white fw-bold " href="#back_to_top">Back To Top</a>&nbsp;<i class="fa fa-arrow-up" aria-hidden="true"></i></h4>
            </button>
        </div>
    </section>
@endsection
@section('script')
    <script>
        function active(el, productType) {
            var elements = document.querySelectorAll('.is_active');
            elements.forEach(function(item) {
                if (item !== el) {
                    item.classList.remove('active');
                }
            });
            el.classList.add("active");

            // Show only the products corresponding to the active button
            var productDivs = document.querySelectorAll('.products_on_active');
            productDivs.forEach(function(div) {
                console.log(div.id, productType);
                if (div.id == productType) {
                    div.classList.remove('d-none');
                } else {
                    div.classList.add('d-none');
                }
            });
        }
    </script>
@endsection
