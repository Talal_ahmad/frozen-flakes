@extends('layouts.master')
@section('title', 'All Feedbacks')
@section('body')
    <section>
        <div class="container">
            <h1 class="text-center my-5 fw-semibold theme_color_pink">All Feedbacks</h1>
            <div class="row">
                @foreach ($feedbacks as $feedback)
                    <div class="col-md-6 mb-4">
                        <div class="card h-100 rounded-2">
                            <div class="card-body">
                                <h5 class="card-title text-primary fw-semibold mb-2">{{ $feedback->name }}</h5>
                                <h6 class="card-subtitle mb-2 text-muted">{{ $feedback->email }}</h6>
                                <p class="card-text"><span class="fw-bold">Phone: </span>{{ $feedback->phone_no }}</p>
                                <p class="card-text"><span class="fw-bold">Satisfaction: </span>
                                    <span class=" fw-semibold {{ $feedback->satisfaction == 0 ? 'text-danger' : 'text-success' }}">
                                        {{ $feedback->satisfaction == 0 ? 'Not Satisfied' : 'Satisfied' }}
                                    </span>
                                </p>
                                <p class="card-text"><span class="fw-bold">Suggestion: </span>{{ $feedback->suggestion }}</p>
                                <p class="card-text"><span class="fw-bold">Submitted At: </span>{{ $feedback->created_at->format('Y-m-d H:i:s') }}</p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection
