@extends('layouts.master')
@section('title', 'About Us')
<style>
    .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

    .about_us_linear_gradient {
        background: linear-gradient(rgba(255, 229, 232, 0.47), rgba(247, 242, 231, 0.2162));
    }

    .custom_position_icon {
        left: 3%;
        top: -6%;
        z-index: 100;
    }

    .custom_p_arrow {
        left: 10%;
        bottom: 10%;
        z-index: 100;
        width: 3% !important;
    }


    @media (min-width: 768px) {
        .bd-placeholder-img-lg {
            font-size: 3.5rem;
        }
    }
</style>
@section('body')
    <section>
        <div class=" p-0" id="back_to_top">
            <img width="100%" src="{{ asset('assets/about_us_new_main.jpg') }}" alt="">
        </div>
        {{-- <div class=" position-relative ">
            <img class=" position-absolute custom_p_arrow" src="{{ asset('assets/arow animation.gif') }}" alt="">
        </div> --}}
        <div class=" text-center">
            <div>
                <p class="solid m-0">Welcome to Frozen Flakes</p>
                <h1 class="ff_comfortaa_new m-0 display-5">Cafe! How did</h1>
                <h2 class="theme_color_pink ff_comfortaa"> we start...</h2>
            </div>
            <div>
                <img class="mt-3" width="100%" src="{{ asset('assets/about_us_main_2.png') }}" alt="">
            </div>
        </div>
        <div class=" about_us_linear_gradient py-5 my-5">
            <div class=" container px-0 px-xl-1">
                <div class="row row-cols-md-2 row-cols-1 ff_popins me-0 ms-xl-5 mx-0 align-items-center">
                    <div class=" ps-xl-5 ps-0 text-center text-md-start position-relative ">
                        {{-- <div class=" position-absolute custom_position_icon">
                            <img class=" w-25" src="{{ asset('assets/about_us_heading_icon.png') }}" alt="">
                        </div> --}}
                        <div>
                            <h1 class=" ff_comfortaa_new font-monospace theme_color_blue m-0">FrozenFlakesCafe</h1>
                            <h1 class="ff_comfortaa_new theme_color_pink font-monospace custom_fs_heading">Our aim
                        </div>
                        </h1>
                        <p class="ff_comfortaa py-4 col-md-11 col-lg-9 col-12">
                            The essence of the cafe lies in the belief that everyone deserves a simple pleasure in life
                            Frozen Flakes Cafe aims to become the ultimate destination for those seeking a delightful sweet
                            treat in Ras Al Khaimah With an extensive selection of over 30 unique ice cream flavors & A
                            range of high-quality coffee and pastries for a complete dining or snacking experience </p>
                    </div>
                    <div class="py-4 text-center ">
                        <img width="100%" height="100%" src="{{ asset('assets/about_us_product_1.png') }}"
                            alt="">
                    </div>
                </div>

                <div class="row row-cols-md-2 row-cols-1 ff_popins me-0 ms-xl-5 mx-0 align-items-center">
                    <div class="py-4 text-md-start text-center  ">
                        <img width="100%" height="100%" src="{{ asset('assets/about_us_product_2.png') }}"
                            alt="">
                    </div>
                    <div class=" ps-xl-5 ps-0 text-center text-md-start position-relative ">
                        {{-- <div class=" position-absolute custom_position_icon">
                            <img class=" w-25" src="{{ asset('assets/about_us_heading_icon.png') }}" alt="">
                        </div> --}}
                        <h1 class=" ff_comfortaa_new font-monospace theme_color_blue m-0">Feel Better</h1>
                        <h1 class="ff_comfortaa_new theme_color_pink font-monospace custom_fs_heading ">with FrozenFlakes
                        </h1>
                        <p class="ff_comfortaa py-4 col-md-11 col-lg-9 col-12">
                            Whether one seeks a moment of relaxation or a quick pick-me-up, Frozen Flakes endeavors to cater
                            to these desires by providing a diverse array of offerings for a satisfying and enjoyable visit.
                    </div>
                </div>
            </div>
        </div>
        <div class=" text-center">
            <h4><button class=" btn btn-secondary theme_color_bg_pink border-0 btn-lg ">
                    <a class=" text-decoration-none text-white fw-bold " href="#back_to_top">Back To Top</a>&nbsp;<i
                        class="fa fa-arrow-up" aria-hidden="true"></i></h4>
            </button>
        </div>
    </section>
@endsection
@section('script')
    <script async src="https://cdnjs.cloudflare.com/ajax/libs/masonry/4.2.2/masonry.pkgd.min.js"
        integrity="sha384-GNFwBvfVxBkLMJpYMOABq3c+d3KnQxudP/mGPkzpZSTYykLBNsZEnG2D9G/X/+7D" crossorigin="anonymous">
    </script>
@endsection
