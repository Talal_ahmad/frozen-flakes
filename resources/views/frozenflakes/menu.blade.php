@extends('layouts.master')
@section('title', 'Menu')
<style>
    .menu-carousel {
        padding: 50px 0 !important;
        background-color: #f8f9fa !important;
        overflow: hidden !important;
    }

    .carousel-item {
        height: 950px !important;
        /* Adjust the height as needed */
        perspective: 1000px !important;
        transform-style: preserve-3d !important;
    }

    .carousel-item.active {
        z-index: 1 !important;
    }

    .carousel-item-prev,
    .carousel-item-next {
        transform: rotateY(0) !important;
        transition: transform 1s !important;
        position: absolute !important;
        top: 0 !important;
        width: 100% !important;
        backface-visibility: hidden !important;
    }

    /* .carousel-item-prev.carousel-item-left,
    .carousel-item-next.carousel-item-right {
        transform: rotateY(-180deg) !important;
    }

    .carousel-item-prev.carousel-item-right,
    .carousel-item-next.carousel-item-left {
        transform: rotateY(180deg) !important;
    } */
</style>

@section('body')
    <section>
        <div class=" p-0">
            <img width="100%" src="{{ asset('assets/menu_new_banner.png') }}" alt="">
        </div>
        <div class=" text-center m-0 py-4">
            <h1 class=" fw-bolder text-secondary ff_comfortaa_new">Explore Our Menu</h1>
        </div>
        <div class="">
            <div id="curlCarousel" class="carousel slide curl-effect d-none d-lg-block " data-bs-ride="carousel">
                <div class="carousel-inner ">
                    <div class="carousel-item active">
                        <a href="{{ asset('products') }}">
                            <img width="100%" height="100%" src="{{ asset('assets/menu_group_1.png') }}" alt="Slide 1">
                        </a>
                    </div>
                    <div class="carousel-item">
                        <a href="{{ asset('products') }}">
                            <img width="100%" height="100%" src="{{ asset('assets/menu_group_2.png') }}" alt="Slide 2">
                        </a>
                    </div>
                    <div class="carousel-item">
                        <a href="{{ asset('products') }}">
                            <img width="100%" height="100%" src="{{ asset('assets/menu_group_3.png') }}" alt="Slide 3">
                        </a>
                    </div>
                </div>
                <button class="carousel-control-prev" type="button" data-bs-target="#curlCarousel" data-bs-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Previous Slide</span>
                </button>
                <button class="carousel-control-next" type="button" data-bs-target="#curlCarousel" data-bs-slide="next">
                    <span class="carousel-control-next-icon text-dark " aria-hidden="true"></span>
                    <span class="visually-hidden">Next Slide</span>
                </button>
            </div>

            <div id="curlCarousel2" class="carousel slide curl-effect d-lg-none d-block " data-bs-ride="carousel">
                <div class="carousel-inner d-block d-lg-none ">
                    <div class="carousel-item active">
                        <a href="{{ asset('products') }}">
                            <img width="100%" height="100%" src="{{ asset('assets/Group 1000001842.png') }}"
                                alt="Slide 1">
                        </a>
                    </div>
                    <div class="carousel-item">
                        <a href="{{ asset('products') }}">
                            <img width="100%" height="100%" src="{{ asset('assets/Group 1000001843.png') }}"
                                alt="Slide 2">
                        </a>
                    </div>
                    <div class="carousel-item">
                        <a href="{{ asset('products') }}">
                            <img width="100%" height="100%" src="{{ asset('assets/Group 1000001844.png') }}"
                                alt="Slide 3">
                        </a>
                    </div>
                    <div class="carousel-item">
                        <a href="{{ asset('products') }}">
                            <img width="100%" height="100%" src="{{ asset('assets/Group 1000001845.png') }}"
                                alt="Slide 4">
                        </a>
                    </div>
                    <div class="carousel-item">
                        <a href="{{ asset('products') }}">
                            <img width="100%" height="100%" src="{{ asset('assets/Group 1000001846.png') }}"
                                alt="Slide 5">
                        </a>
                    </div>
                    <div class="carousel-item">
                        <a href="{{ asset('products') }}">
                            <img width="100%" height="100%" src="{{ asset('assets/Group 1000001847.png') }}"
                                alt="Slide 6">
                        </a>
                    </div>
                </div>
                <button class="carousel-control-prev" type="button" data-bs-target="#curlCarousel2" data-bs-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Previous Slide</span>
                </button>
                <button class="carousel-control-next" type="button" data-bs-target="#curlCarousel2" data-bs-slide="next">
                    <span class="carousel-control-next-icon text-dark " aria-hidden="true"></span>
                    <span class="visually-hidden">Next Slide</span>
                </button>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script></script>
@endsection
