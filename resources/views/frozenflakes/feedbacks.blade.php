@extends('layouts.master')
@section('title', 'Feedbacks')
<style>
    button #submit {
        background-color: #F8939C !important;
        cursor: pointer;
        transition: .5s ease-in-out;
    }

    #submit:hover {
        transform: translateY(-10px);
        color: #fff !important;
        background-color: rgb(108, 117, 125) !important;
    }

    .feedback_bg_cust {
        background-image: url('{{ asset('assets/pink-ice-cream-cone-with-pink-background_662214-23036 1.png') }}');
        background-position: center !important;
        background-size: 100% 100% !important;
        background-repeat: no-repeat !important;
    }

    .theme_color_bg_trans {
        background-color: transparent !important
    }

    .form-check-input:checked {
        background-color: gray !important;
        border-color: grey !important;
    }

    .form-control:focus {
        border-color: gray !important;
        box-shadow: 0 0 0 0.25rem gray !important;
    }
</style>
@section('body')
    <section class=" d-flex justify-content-center feedback_bg_cust">
        <div class="theme_color_bg_trans form-text w-md-50 w-100 mx-1 shadow p-4 rounded my-5" style="max-width: 500px;">
            <div class="text-center textup fs-4  text-secondary ">
                <i class="fas fa-clock text-secondary "></i>
                It only takes two minutes!!
            </div>
            <form class="mt-4" id="feedback_form" method="POST" action="{{ url('/submit_feedbacks') }}">
                @csrf
                <div class="mb-3">
                    <label for="name" class="form-label"><i class="fas fa-user text-secondary "></i> Name</label>
                    <input type="text" class="form-control" id="name" name="name" required>
                </div>

                <div class="mb-3">
                    <label for="email" class="form-label"><i class="fas fa-envelope text-secondary "></i> Email
                        Address</label>
                    <input type="email" class="form-control" id="email" name="email" required>
                </div>

                <div class="mb-3">
                    <label for="phone_no" class="form-label"><i class="fas fa-phone text-secondary "></i> Phone No</label>
                    <input type="tel" class="form-control" id="phone_no" name="phone_no" required>
                </div>

                <div class="mb-3">
                    <label class="form-label"><i class="fas fa-smile text-secondary "></i> Are you satisfy with our
                        service?</label>
                    <div class=" text-center">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input fs-6" type="radio" id="yes" name="satisfaction"
                                value="1" checked>
                            <label class="form-check-label fs-6" for="yes">Yes</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input fs-6" type="radio" id="no" name="satisfaction"
                                value="0">
                            <label class="form-check-label fs-6" for="no">No</label>
                        </div>
                    </div>
                </div>

                <div class="mb-3">
                    <label for="suggestion" class="form-label"><i class="fas fa-comments text-secondary "></i> Write your
                        Suggestions:</label>
                    <textarea class="form-control" id="suggestion" name="suggestion" rows="4" required></textarea>
                </div>
                <div class=" text-center ">
                    <button type="submit"
                        class="btn text-dark bg-transparent border-secondary border-2 shadow fw-semibold w-50 mt-3 rounded-pill "
                        style="transition: background-color 0.2s, transform 0.2s;" id="submit">Submit</button>
                </div>
            </form>
        </div>
    </section>
@endsection
