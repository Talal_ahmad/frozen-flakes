@extends('layouts.master')
@section('title', 'Frozen Flakes')
<style>
    body {
        /* font-family: 'Comfortaa', sans-serif; */
    }

    /* .body-bg {
        background-position: center !important;
        background-size: 100% !important;
        background-repeat: no-repeat !important;
    } */

    .btn:focus {
        outline: 0;
        box-shadow: 0 0 0 0.2rem #a582827b !important;
    }

    /* .main-bg {
        background-position: center;
        background-size: 100% 100%;
        background-repeat: no-repeat;
    } */

    .card {
        position: relative;
        overflow: hidden;
    }

    .card-img-container {
        position: relative;
        overflow: hidden;
        height: 200px;
        transition: filter 0.3s ease;
    }

    .card-img-container::before {
        content: '';
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        filter: blur(10px);
        z-index: -1;
        transition: filter 0.5s ease;
        /* Corrected transition property */
    }

    .card:hover .card-img-top {
        opacity: 0.5;
        filter: blur(2px);
        cursor: pointer;
    }

    .card-body {
        position: absolute;
        bottom: -100%;
        left: 0;
        width: 100%;
        padding: 1rem;
        background-color: rgba(255, 255, 255, 0.9);
        transition: bottom 0.3s ease;
    }

    .card:hover .card-body {
        bottom: 0;
    }

    .card-title,
    .card-text {
        transition: opacity 0.3s ease;
        opacity: 0;
    }

    .card:hover .card-title,
    .card:hover .card-text {
        opacity: 1;
    }

    .card {
        height: 300px !important;
    }

    .about_us_section {
        background-image: url('{{ asset('assets/Union.png') }}') !important;
        background-position: center !important;
        background-size: 100% 100% !important;
        background-repeat: no-repeat !important;

    }

    .specials_us_section {
        background-image: url('{{ asset('assets/Rectangle 35.png') }}') !important;
        background-position: center !important;
        background-size: 100% 100% !important;
        background-repeat: no-repeat !important;

    }

    .color_peach {
        background-color: #FEA967 !important;
        color: white !important;
    }

    .nav_tabs_product {
        background-color: #FBF1F2 !important;
        border: none;
    }

    .nav_tabs_product.active {
        background-color: #F8939C !important;
        color: white !important;
    }

    .carousel-indicators [data-bs-target] {
        background-color: #000 !important;
    }

    .carousel-indicators .active {
        background-color: #f95260 !important;
    }

    .fs_custom_comments {
        font-size: 1rem !important;
    }

    .fs_custom_comments_top {
        font-size: 1.25rem !important;
    }

    .btn_fs_menu {
        font-size: 1rem !important;
    }

    #about_us:hover #about_us_content {
        display: block !important;
    }

    .custom_mb {
        margin-bottom: -13px !important
    }

    .ff_comfortaa_new {
        font-family: emoji !important;
    }

    .ff_comfortaa_new_fs {
        font-size: 32px;
    }

    .fs_i {
        font-size: 1.25rem !important;
    }

    .fs_4_cust {
        font-size: calc(1.275rem + .3vw) !important;
    }

    @media only screen and (min-width: 1200px) {
        .ff_comfortaa_new_fs {
            font-size: 50px !important;
        }

        .ff_comfortaa_new_fs_1_15 {
            font-size: 50px !important;
        }
    }

    @media only screen and (max-width: 768px) {
        .custom_fs_heading {
            font-size: 1.9rem !important;
            /* font-weight: 500 !important; */
        }

        .news_heading_fs {
            font-size: 1.9rem !important;
            /* font-weight: 500 !important; */
        }

        .fs_custom_comments_top {
            font-size: 1rem !important;
        }

        .fs_custom_comments {
            font-size: 0.7rem !important;
        }

        .btn_fs_menu {
            font-size: 0.7rem !important;
        }

        .custom-nav-btn-width {
            width: 6vw !important;
        }
    }

    @media only screen and (max-width: 576px) {
        .custom-nav-btn-width {
            width: 8vw !important;
        }
    }

    @media only screen and (max-width: 425px) {
        .fs_menu_heading {
            font-size: 1.4rem !important;
        }

        .custom-nav-btn-width {
            width: 11vw !important;
        }
    }

    @media only screen and (max-width: 375px) {
        .fs_custom_comments_top {
            font-size: 0.7rem !important;
        }

        .fs_custom_comments {
            font-size: 0.5rem !important;
        }

        .btn_fs_menu {
            font-size: 0.7rem !important;
        }

        .fs_menu_heading {
            font-size: 1.2rem !important;
        }

        .custom-nav-btn-width {
            width: 13vw !important;
        }

        .custom_fs {
            font-size: 1.15rem !important;
        }

        .custom_fs_0_7 {
            font-size: 0.8rem !important;
        }

        .ff_comfortaa_new_fs {
            font-size: 1.4rem !important;
        }

        p {
            font-size: 0.8rem !important;
        }

        .ff_comfortaa_new_fs_1_15 {
            font-size: 1.15rem !important;
        }

        .custom_img_testimonials_width {
            width: 30% !important;
        }

        .carouselExampleAutoplaying_testimonials_cust_width {
            width: 65% !important;
        }

        .card-img-top {
            height: 100vh !important;
        }
    }

    @media only screen and (max-width: 320px) {
        .fs_menu_heading {
            font-size: 1rem !important;
        }

        .custom-nav-btn-width {
            width: 15 vw !important;
        }
    }
</style>
@section('body')
    <section>
        <div class=" body-bg">
            <img class=" position-relative custom_position_btn" style="z-index: -1" width="100%"
                src="{{ asset('assets/home_main_banner.png') }}" alt="">
            {{-- <div class=" ms-lg-5 ms-1 mt-5">
                <div class="row me-0 ms-xl-5 ms-auto solid align-items-center ">
                    <div class=" ps-xl-5 ps-auto col-md-5 col-12 text-center text-md-start ">
                        <p class="theme_color_pink font-weight-normal ff_popins fw-semibold">Discover Symphony of
                            flavors</p>
                        <h1 class=" ff_comfortaa font-monospace custom_fs_heading">Indulge In Irresistible</h1>
                        <h1 class="ff_comfortaa theme_color_pink font-monospace"> Frozen Flakes Café </h1>
                        <p class="solid my-4 col-xl-8 col-lg-11 col-11">
                            At Soft, our team is deeply passionate about meticulously curating and crafting
                            exceptional
                            ice cream experiences that leave a lasting impression on all who indulge in our
                            creations.
                        </p>
                        <div class=" d-flex align-items-center justify-content-center justify-content-md-start ">
                            <a href="{{ asset('menu') }}">
                                <button class="btn contact_us_btn rounded-pill px-xl-5 px-3 py-3 me-2">Our
                                    Menu</button>
                            </a>
                            {{-- <button class="btn btn-outline-secondary rounded-pill  px-xl-5 px-3 py-3 ms-2">Shop Now</button> --}}
            {{-- </div>
                    </div>
                    <div class="p-0 m-0 col-md-7 col-12">
                        <img width="100%" height="100%" src="{{ asset('assets/Yogurt.png') }}" alt="">
                    </div>
                </div>
            </div> --}}
            <div class=" bg-secondary-subtle theme_color_pinks text-center py-2 mb-5 custom_fs_0_7"><b>All day dining,
                    speciality
                    coffee
                </b><i class="ff_comfortaa_new fs_i custom_fs_0_7">and handmade</i><b> patisserie
                </b><i class="ff_comfortaa_new fs_i custom_fs_0_7"> – elevating the everyday</i></div>
        </div>
        <div class=" text-center py-2 mb-1">
            <p class="ff_popins theme_color_pink fw-bold ">TRENDING FLAVORS</p>
            <h2 class=" ff_comfortaa_new ff_comfortaa_new_fs">Explore Different Kind Of Flavor</h2>
        </div>
        <div class="container p-0 p-xl-1">
            <div class=" row row-cols-lg-3 row-cols-md-2 m-0">
                <div>
                    <div class="card mx-1 my-3 border-0 ">
                        <img src="{{ asset('assets/home_menu_card1.jpg') }}" class="card-img-top h-100" alt="...">
                        <div class="card-body" style="background-color: #ffd5d9">
                            <h5 class="card-title fw-bolder fs_4_cust text-secondary custom_fs">Salted Caramel Ice Cream
                            </h5>
                            {{-- <p class="card-text text-secondary fs-6 ">Savor the classic elegance of <b>Vanilla Bliss</b>
                                –
                                rich, creamy Madagascar vanilla in every scoop. A luxurious treat for the vanilla
                                connoisseur. 🍨</p> --}}
                        </div>
                    </div>
                </div>
                <div>
                    <div class="card mx-1 my-3 border-0 ">
                        <img src="{{ asset('assets/home_menu_card2.jpg') }}" class="card-img-top h-100" alt="...">
                        <div class="card-body" style="background-color: #ffd5d9">
                            <h5 class="card-title fw-bolder fs_4_cust text-secondary custom_fs">Spanish Latte</h5>
                            {{-- <p class="card-text text-secondary fs-6 ">Savor the classic elegance of <b>Caramel Bliss</b>
                                –
                                rich, creamy Madagascar caramel in every scoop. A luxurious treat for the caramel
                                connoisseur. 🍨</p> --}}
                        </div>
                    </div>
                </div>
                <div>
                    <div class="card mx-1 my-3 border-0 ">
                        <img src="{{ asset('assets/home_menu_card3.jpg') }}" class="card-img-top h-100 " alt="...">
                        <div class="card-body" style="background-color: #ffd5d9">
                            <h5 class="card-title fw-bolder fs_4_cust text-secondary custom_fs">Healthy Frozen Yoghurts</h5>
                            {{-- <p class="card-text text-secondary fs-6 ">Savor the classic elegance of <b>Strawberry
                                    Bliss</b>
                                –
                                rich, creamy Madagascar strawberry in every scoop. A luxurious treat for the strawberry
                                connoisseur. 🍨</p> --}}
                        </div>
                    </div>
                </div>
            </div>
            <div class=" my-4 text-center ">
                <a href="{{ asset('products') }}">
                    <button class="btn contact_us_btn rounded-pill py-2 px-4 fw-semibold fs-6">Our Store</button>
                </a>
            </div>
        </div>
        <div class=" about_us_section my-5 py-5">
            <div class=" container py-5 p-xl-1 p-0">
                <div class="row row-cols-md-2 row-cols-1 ff_popins me-0 ms-xl-5 ms-auto align-items-center py-5">
                    <div class=" ps-xl-5 ps-1 text-center text-md-start ">
                        <p class=" text-secondary font-weight-normal ff_popins fw-semibold">About Us</p>
                        <h1 class=" ff_comfortaa_new ff_comfortaa_new_fs custom_fs_heading">Crafting Happiness,<span
                                class="ff_comfortaa_new ff_comfortaa_new_fs theme_color_pink">One</span></h1>
                        <h1 class="ff_comfortaa_new ff_comfortaa_new_fs theme_color_pink"> Scoop At A Time</h1>
                        <p class="ff_comfortaa my-4 col-lg-10 col-md-11 col-xxl-12">
                            Discover Our Exclusive Seasonal Specials And Limited-Time Creations. Our Chefs Are
                            Constantly Innovating, Bringing You Unique And Exciting Flavors To Savor. Don't Miss
                            Out On
                            These Extraordinary Treats - They're Here For A Limited Time Only!
                        </p>
                        <div class=" d-flex justify-content-center justify-content-md-start ">
                            <a href="{{ asset('about_us') }}">
                                <button class="btn contact_us_btn rounded-pill px-5 px-md-4 py-2 mx-2">About Us</button>
                            </a>
                            {{-- <button class="btn btn-outline-secondary rounded-pill  px-5 px-md-4 py-2 mx-2">Shop
                                Now</button> --}}
                        </div>
                    </div>
                    <div class="p-0 m-0">
                        <img width="100%" height="100%" src="{{ asset('assets/Coffee.png') }}" alt="">
                    </div>
                </div>
            </div>
        </div>

        <div class=" text-center my-md-5 mb-sm-3 mb-0 mt-5 pt-5">
            <p class="ff_popins theme_color_pink fw-bold ">MENU</p>
            <h2 class="ff_comfortaa_new ff_comfortaa_new_fs_1_15 ">Delightful Selections For Every Palate
            </h2>
        </div>
        {{-- <ul class="nav nav-pills text-center d-flex justify-content-center mb-lg-5 mb-sm-3">
            <li class="nav-item mx-2 mx-sm-1 mt-2 mt-sm-auto">
                <button
                    class="nav_tabs_product btn is_active active rounded-4 py-md-3 px-m-4 py-2 px-3 btn_fs_menu fw-semibold"
                    onclick="active(this)">Cup Cake</button>
            </li>
            <li class="nav-item mx-2 mx-sm-1 mt-2 mt-sm-auto">
                <button class="nav_tabs_product btn is_active rounded-4 py-md-3 px-m-4 py-2 px-3 btn_fs_menu fw-semibold"
                    onclick="active(this)">Ice
                    Coffee</button>
            </li>
            <li class="nav-item mx-2 mx-sm-1 mt-2 mt-sm-auto">
                <button class="nav_tabs_product btn is_active rounded-4 py-md-3 px-m-4 py-2 px-3 btn_fs_menu fw-semibold"
                    onclick="active(this)">Flavorful bar</button>
            </li>
            <li class="nav-item mx-2 mx-sm-1 mt-2 mt-sm-auto">
                <button class="nav_tabs_product btn is_active rounded-4 py-md-3 px-m-4 py-2 px-3 btn_fs_menu fw-semibold"
                    onclick="active(this)">Choco bar</button>
            </li>
            <li class="nav-item mx-2 mx-sm-1 mt-2 mt-sm-auto">
                <button class="nav_tabs_product btn is_active rounded-4 py-md-3 px-m-4 py-2 px-3 btn_fs_menu fw-semibold"
                    onclick="active(this)">Ice
                    Cream</button>
            </li>
        </ul> --}}
        <div class=" container ">
            <div id="carouselExampleAutoplaying" class="carousel slide" data-bs-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item">
                        <div class="d-flex">
                            <div class="card mx-3 border-0 my-3">
                                <a href="{{ asset('menu') }}">
                                    <img src="{{ asset('assets/Rectangle 12.jpg') }}" class="card-img-top " alt="...">
                                </a>
                            </div>
                            <div class="card mx-3 border-0 my-3 d-none d-md-block ">
                                <a href="{{ asset('menu') }}">
                                    <img src="{{ asset('assets/Rectangle 13.jpg') }}" class="card-img-top " alt="...">
                                </a>
                            </div>
                            <div class="card mx-3 border-0 my-3 d-none d-lg-block ">
                                <a href="{{ asset('menu') }}">
                                    <img src="{{ asset('assets/Rectangle 14.jpg') }}" class="card-img-top " alt="...">
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item active ">
                        <div class="d-flex">
                            <div class="card mx-3 border-0 my-3">
                                <a href="{{ asset('menu') }}">
                                    <img src="{{ asset('assets/bakery10.jpg') }}" class="card-img-top " alt="...">
                                </a>
                            </div>
                            <div class="card mx-3 border-0 my-3 d-none d-md-block ">
                                <a href="{{ asset('menu') }}">
                                    <img src="{{ asset('assets/bakery2.jpg') }}" class="card-img-top " alt="...">
                                </a>
                            </div>
                            <div class="card mx-3 border-0 my-3 d-none d-lg-block ">
                                <a href="{{ asset('menu') }}">
                                    <img src="{{ asset('assets/bakery7.jpg') }}" class="card-img-top " alt="...">
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="d-flex">
                            <div class="card carousel_cards mx-3 border-0 rounded-0 my-3">
                                <a href="{{ asset('menu') }}">
                                    <img src="{{ asset('assets/pastry1.jpg') }}" class="card-img-top" alt="...">
                                </a>
                            </div>
                            <div class="card mx-3 border-0 my-3 d-none d-md-block ">
                                <a href="{{ asset('menu') }}">
                                    <img src="{{ asset('assets/coldcoffee1.jpg') }}" class="card-img-top "
                                        alt="...check">
                                </a>
                            </div>
                            <div class="card mx-3 border-0 my-3 d-none d-lg-block ">
                                <a href="{{ asset('menu') }}">
                                    <img src="{{ asset('assets/hotcoffee3.jpg') }}" class="card-img-top "
                                        alt="...">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="carousel-indicators custom_mb">
                    <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0"
                        aria-label="Slide 1"></button>
                    <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1"
                        class="active" aria-current="true" aria-label="Slide 2"></button>
                    <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2"
                        aria-label="Slide 3"></button>
                </div>
                <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleAutoplaying"
                    data-bs-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true">
                        <img class=" w-100" src="{{ asset('assets/nav-arrows.png') }}" alt="Previous">
                    </span>
                    <span class="visually-hidden">Previous</span>
                </button>
                <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleAutoplaying"
                    data-bs-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true">
                        <img class=" w-100" src="{{ asset('assets/nav-arrow2.png') }}" alt="next">
                    </span>
                    <span class="visually-hidden">Next</span>
                </button>
            </div>
        </div>
        {{-- <div class=" specials_us_section mt-md-5 mt-5 mb-0">
            <div class=" container py-4 px-0 px-xl-1">
                <div class="row row-cols-md-2 row-cols-1 ff_popins me-0 ms-xl-5 mx-0 align-items-center py-5">
                    <div class=" ps-xl-5 ps-0 text-center text-md-start ">
                        <p class=" text-secondary font-weight-normal ff_popins fw-semibold">SPECIALS</p>
                        <h1 class=" ff_comfortaa font-monospace ">Seasonal Sensations</h1>
                        <h1 class="ff_comfortaa theme_color_pink font-monospace custom_fs_heading"> And Limited-Time Treats
                        </h1>
                        <p class="ff_comfortaa my-4 col-md-11 col-12">
                            <span class=" text-primary ">Frozenflakescafe,</span> We Are Passionate About Creating
                            Unforgettable Ice Cream Experiences. Our
                            Journey Begon With A Love For Quality Ingredients And A Desire To Share The Joy Of
                            Exceptional Flavors.
                        </p>
                        <a href="{{ asset('menu') }}">
                            <button class="btn color_peach rounded-pill px-5 px-md-4 py-3 mx-2 fw-semibold ">Try Out Our
                                Specials</button>
                        </a>
                    </div>
                    <div class="p-0 m-0">
                        <img width="100%" height="100%" src="{{ asset('assets/Ice-cream-main.png') }}"
                            alt="">
                    </div>
                </div>
            </div>
        </div> --}}

        {{-- <div class=" news_section mb-md-5 mt-0 pt-md-5" style="background-color: #F9F9F9;">
            <div class=" container py-5" id="feedbacks">
                <div
                    class="row row-cols-md-2 row-cols-1 text-center text-md-start ff_popins me-0 ms-xl-5 mx-0 align-items-start pt-5 pb-2 py-md-3">
                    <div>
                        <img width="100%" height="100%" src="{{ asset('assets/last-img.png') }}" alt="">
                    </div>
                    <div class="mt-xl-5 mt-3 mt-md-2 ps-lg-5 px-md-1 ">
                        <h1 class=" ff_comfortaa font-monospace news_heading_fs">Recent News
                            <span class="ff_comfortaa theme_color_pink font-monospace"> & Updates</span>
                        </h1>
                        <div class="row align-items-start mt-5 mt-md-3">
                            <div class="row col-12 mb-4 mb-sm-5 mb-md-4">
                                <div class=" col-5 bg-white p-3 rounded-5">
                                    <img class="w-100 text-center " src="{{ asset('assets/last-img-2.png') }}"
                                        class="card-img-top" alt="...">
                                </div>
                                <div class=" col-7 d-flex align-items-center ">
                                    <div class=" ms-4 text-start text-truncate">
                                        <p
                                            class=" ff_comfortaa text-body-secondary fs_custom_comments_top py-2 text-truncate m-0 m-sm-1 ">
                                            The
                                            service was wonderful. Our waiter was so attentive and accommodating.
                                        </p>
                                        <div>
                                            <span class=" theme_color_pink d-flex align-items-baseline py-1 m-0 m-sm-1 ">
                                                <i class="fa-solid fa-user">
                                                </i>
                                                <p class=" ms-2 text-dark fs_custom_comments m-0 m-sm-1 ">By <span
                                                        class="theme_color_pink fw-bold ">Fay Simpson</span></p>
                                            </span>
                                            <span
                                                class=" theme_color_pink d-flex align-items-md-baseline py-2 m-0 m-sm-1 ">
                                                <i class="fa-solid fa-comment">
                                                </i>
                                                <p class=" ms-2 text-dark fs_custom_comments">
                                                    <b>dobirary@mailinator.com</b>
                                                </p>
                                            </span>
                                        </div>
                                        {{-- <div class=" mt-xl-3 mt-auto ">
                                            <a href="{{ asset('view_feedbacks') }}">
                                                <button
                                                    class="btn contact_us_btn rounded-pill px-3 px-md-4 py-1 py-sm-2 py-md-1 mx-0 text-light fs_custom_comments_top">Read
                                                    More</button>
                                            </a>
                                        </div> --}}
        {{-- </div>
                                </div>
                            </div>
                            <div class="row col-12 mb-4 mb-sm-5 mb-md-4">
                                <div class=" col-5 bg-white p-3 rounded-5">
                                    <img class="w-100 text-center " src="{{ asset('assets/last-img-2.png') }}"
                                        class="card-img-top" alt="...">
                                </div>
                                <div class=" col-7 d-flex align-items-center">
                                    <div class=" ms-4 text-start text-truncate ">
                                        <p
                                            class=" ff_comfortaa text-body-secondary fs_custom_comments_top py-2 text-truncate m-0 m-sm-1 ">
                                            Overall, this was an incredible dining experience. </p>
                                        <div>
                                            <span class=" theme_color_pink d-flex align-items-baseline py-1 m-0 m-sm-1 ">
                                                <i class="fa-solid fa-user">
                                                </i>
                                                <p class=" ms-2 text-dark fs_custom_comments m-0 m-sm-1">By <span
                                                        class="theme_color_pink fw-semibold ">Shaeleigh Bowen</span></p>
                                            </span>
                                            <span class=" theme_color_pink d-flex align-items-baseline py-2 m-0 m-sm-1 ">
                                                <i class="fa-solid fa-comment">
                                                </i>
                                                <p class=" ms-2 text-dark fs_custom_comments">
                                                    <b>gicuxage@mailinator.com</b>
                                                </p>
                                            </span>
                                        </div>
                                        {{-- <div class=" mt-xl-3 mt-auto">
                                            <a href="{{ asset('view_feedbacks') }}">
                                                <button
                                                    class="btn contact_us_btn rounded-pill px-3 px-md-4 py-1 py-sm-2 py-md-1 mx-0 text-light fs_custom_comments_top">Read
                                                    More</button>
                                            </a>
                                        </div> --}}
        {{-- </div>
                                </div>
                            </div>
                        </div>
                        <div class=" text-center ">
                            <a href="{{ asset('view_feedbacks') }}">
                                <button
                                    class="btn contact_us_btn rounded-pill px-3 px-md-4 py-1 py-sm-2 py-md-1 mx-0 text-light fs_custom_comments_top">View
                                    All Feedbacks</button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div> --}}
        <section class=" py-sm-5 py-4 my-5 bg-body-secondary ">
            <h1 class=" text-center mb-sm-5 mb-3 ff_comfortaa_new fw-bold theme_color_pink">
                Testimonials</h1>
            <div id="carouselExampleAutoplaying_testimonials" class="carousel slide" data-bs-ride="carousel">
                <div class="carousel-inner text-center w-75 mx-auto ">
                    <div class="carousel-item active">
                        <div class="">
                            <img src="{{ asset('assets/frozenflakes_logo.png') }}"
                                class=" rounded-circle mb-3 custom_img_testimonials_width" />
                            <h4><b>Muhammed Salih Ahamed Athaullah</b></h4>
                            <p class=" w-75 mx-auto ">One of the best Ice-cream shop in RAK. verity of the ice-creams are
                                good. more verity of flavors available.
                                Service is well and good much appreciated.
                                Very attractive dine in space inside the shop.
                                Parking is available.must visit place. recommended.

                                Suggest the shop owner to increase the verity of the menu like milkshake, waffle with ice
                                cream ect. Hot chocolate need improvement.</p>
                            <h6 class="fs_custom_comments_top"><b>Submitted At: 03-22-2024 09:50:40</b></h6>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="">
                            <img src="{{ asset('assets/frozenflakes_logo.png') }}"
                                class=" rounded-circle mb-3 custom_img_testimonials_width" />
                            <h4><b>Heidi Boor</b></h4>
                            <p class=" w-75 mx-auto ">Wow, this is truly the best ice cream I’ve had in RAK and even Dubai!
                                It tastes like true gelato from Italy—rich, creamy, and so fresh! I had the dark chocolate
                                and Oreo flavors while my husband got the snickers and cheesecake. All we’re amazing.
                                Sometimes at other ice cream shops, the Oreo cookies can be stale or soft from being in the
                                ice cream too long, but neither was the case here! We got it delivered and have found our
                                new favorite treat! Excited to visit the shop in person next time!</p>
                            <h6 class="fs_custom_comments_top"><b>Submitted At: 02-22-2024 03:12:08</b></h6>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="">
                            <img src="{{ asset('assets/frozenflakes_logo.png') }}"
                                class=" rounded-circle mb-3 custom_img_testimonials_width" />
                            <h4><b>Saaim Ibrahim</b></h4>
                            <p class=" w-75 mx-auto ">Amazing experience overall. They have a wide variety of ice creams
                                and also have options for sugar free, lactose free, gluten free. The place inside looks
                                really nice. I highly recommend going to this place if you are in Ras Al Khaimah.</p>
                            <h6 class="fs_custom_comments_top"><b>Submitted At: 02-22-2024 011:02:53</b></h6>
                        </div>
                    </div>
                </div>
                <button class="carousel-control-prev" type="button"
                    data-bs-target="#carouselExampleAutoplaying_testimonials" data-bs-slide="prev">
                    <span aria-hidden="true">
                        <img class="carouselExampleAutoplaying_testimonials_cust_width"
                            src="{{ asset('assets/previous.png') }}" alt="">
                    </span>
                    <span class="visually-hidden">Previous</span>
                </button>
                <button class="carousel-control-next" type="button"
                    data-bs-target="#carouselExampleAutoplaying_testimonials" data-bs-slide="next">
                    <span aria-hidden="true"><img class="carouselExampleAutoplaying_testimonials_cust_width"
                            src="{{ asset('assets/next.png') }}" alt=""></span>
                    <span class="visually-hidden">Next</span>
                </button>
            </div>
            <div class=" text-center mt-4">
                <a href="{{ asset('view_feedbacks') }}">
                    <button
                        class="btn contact_us_btn rounded-pill px-3 px-md-4 py-1 py-sm-2 py-md-1 mx-0 text-light fs_custom_comments_top">View
                        All Testimonials</button>
                </a>
            </div>
        </section>
    </section>
@endsection
@section('script')
    <script>
        function active(el) {
            var elements = document.querySelectorAll('.is_active');
            elements.forEach(function(item) {
                if (item !== el) {
                    item.classList.remove('active');
                }
            });
            el.classList.add("active");
        }
    </script>
@endsection
